﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp_CascadingValues.Utils
{
    public class Config
    {
        public string CorDeFundo { get; set; } = "cyan";
        public string TamanhoFonte { get; set; } = "60px";
    }
}
