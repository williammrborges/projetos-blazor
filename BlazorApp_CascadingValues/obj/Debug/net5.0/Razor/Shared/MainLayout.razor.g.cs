#pragma checksum "C:\Users\william\Desktop\Projetos\BlazorApp_CascadingValues\Shared\MainLayout.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "000fd05beb5a5bdaaea04d3472c60be785e5ca68"
// <auto-generated/>
#pragma warning disable 1591
namespace BlazorApp_CascadingValues.Shared
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Users\william\Desktop\Projetos\BlazorApp_CascadingValues\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\william\Desktop\Projetos\BlazorApp_CascadingValues\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\william\Desktop\Projetos\BlazorApp_CascadingValues\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\william\Desktop\Projetos\BlazorApp_CascadingValues\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\william\Desktop\Projetos\BlazorApp_CascadingValues\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\william\Desktop\Projetos\BlazorApp_CascadingValues\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\william\Desktop\Projetos\BlazorApp_CascadingValues\_Imports.razor"
using Microsoft.AspNetCore.Components.WebAssembly.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\william\Desktop\Projetos\BlazorApp_CascadingValues\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\william\Desktop\Projetos\BlazorApp_CascadingValues\_Imports.razor"
using BlazorApp_CascadingValues;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Users\william\Desktop\Projetos\BlazorApp_CascadingValues\_Imports.razor"
using BlazorApp_CascadingValues.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\Users\william\Desktop\Projetos\BlazorApp_CascadingValues\_Imports.razor"
using BlazorApp_CascadingValues.Utils;

#line default
#line hidden
#nullable disable
    public partial class MainLayout : LayoutComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenElement(0, "div");
            __builder.AddAttribute(1, "class", "page");
            __builder.AddAttribute(2, "b-l86d8dryfe");
            __builder.OpenElement(3, "div");
            __builder.AddAttribute(4, "class", "sidebar");
            __builder.AddAttribute(5, "b-l86d8dryfe");
            __builder.OpenComponent<BlazorApp_CascadingValues.Shared.NavMenu>(6);
            __builder.CloseComponent();
            __builder.CloseElement();
            __builder.AddMarkupContent(7, "\r\n\r\n    ");
            __builder.OpenElement(8, "div");
            __builder.AddAttribute(9, "class", "main");
            __builder.AddAttribute(10, "b-l86d8dryfe");
            __builder.AddMarkupContent(11, "<div class=\"top-row px-4\" b-l86d8dryfe><a href=\"http://blazor.net\" target=\"_blank\" class=\"ml-md-auto\" b-l86d8dryfe>About</a></div>\r\n\r\n        ");
            __builder.OpenElement(12, "div");
            __builder.AddAttribute(13, "class", "content px-4");
            __builder.AddAttribute(14, "b-l86d8dryfe");
            __Blazor.BlazorApp_CascadingValues.Shared.MainLayout.TypeInference.CreateCascadingValue_0(__builder, 15, 16, 
#nullable restore
#line 14 "C:\Users\william\Desktop\Projetos\BlazorApp_CascadingValues\Shared\MainLayout.razor"
                                    config

#line default
#line hidden
#nullable disable
            , 17, (__builder2) => {
                __builder2.AddContent(18, 
#nullable restore
#line 15 "C:\Users\william\Desktop\Projetos\BlazorApp_CascadingValues\Shared\MainLayout.razor"
                 Body

#line default
#line hidden
#nullable disable
                );
            }
            );
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
        }
        #pragma warning restore 1998
#nullable restore
#line 21 "C:\Users\william\Desktop\Projetos\BlazorApp_CascadingValues\Shared\MainLayout.razor"
       

    Config config = new Config();


#line default
#line hidden
#nullable disable
    }
}
namespace __Blazor.BlazorApp_CascadingValues.Shared.MainLayout
{
    #line hidden
    internal static class TypeInference
    {
        public static void CreateCascadingValue_0<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, TValue __arg0, int __seq1, global::Microsoft.AspNetCore.Components.RenderFragment __arg1)
        {
        __builder.OpenComponent<global::Microsoft.AspNetCore.Components.CascadingValue<TValue>>(seq);
        __builder.AddAttribute(__seq0, "Value", __arg0);
        __builder.AddAttribute(__seq1, "ChildContent", __arg1);
        __builder.CloseComponent();
        }
    }
}
#pragma warning restore 1591
