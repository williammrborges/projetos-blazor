﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Blazor_Forms.Models
{
    public class Usuario
    {
        public int Id { get; set; }

        [Required(ErrorMessage ="O nome deve ser informado")]
        [StringLength(10,ErrorMessage ="O nome não pode exceder 10 caracteres")]        
        public string Nome { get; set; }

        [Required(ErrorMessage = "O e-mail deve ser informado")]
        [EmailAddress(ErrorMessage ="Formato de e-mail é inválido")]
        public string Email { get; set; }
        
        [Range(18,80, ErrorMessage ="A idade deve estar entre 18 e 80")]
        public int Idade { get; set; }
    }
}
