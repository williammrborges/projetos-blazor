﻿using Microsoft.AspNetCore.Components.Authorization;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Catalogo_Blazor.Client.Auth
{
    public class DemoAuthStateProvider : AuthenticationStateProvider
    {
        public async override Task<AuthenticationState> GetAuthenticationStateAsync()
        {
            //Indicamos se o usuário esta autenticado e também os seus claims

            var usuario = new ClaimsIdentity(new List<Claim>() { 
                new Claim("Chave", "Valor"),
                new Claim(ClaimTypes.Name, "William Borges"),
                new Claim(ClaimTypes.Role, "Admin")
            }, "demo");

            return await Task.FromResult(new AuthenticationState(
                new ClaimsPrincipal(usuario)
                ));
        }
    }
}
