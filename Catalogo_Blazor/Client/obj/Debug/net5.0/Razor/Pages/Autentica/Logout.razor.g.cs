#pragma checksum "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\Pages\Autentica\Logout.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "6ee901af27f5f152d74efc9dce92dbd2471210d1"
// <auto-generated/>
#pragma warning disable 1591
namespace Catalogo_Blazor.Client.Pages.Autentica
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.WebAssembly.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using Catalogo_Blazor.Client;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using Catalogo_Blazor.Client.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using Catalogo_Blazor.Shared.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 14 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using Catalogo_Blazor.Client.Auth;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using System.Net.Http.Headers;

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using System.Text;

#line default
#line hidden
#nullable disable
#nullable restore
#line 17 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using System.Text.Json;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/logout")]
    public partial class Logout : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.AddMarkupContent(0, "<h4>Logout</h4>\r\n\r\n");
            __builder.OpenComponent<Catalogo_Blazor.Client.Shared.Aviso>(1);
            __builder.AddAttribute(2, "Exibir", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 7 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\Pages\Autentica\Logout.razor"
               Exibir

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(3, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder2) => {
                __builder2.OpenElement(4, "h5");
                __builder2.AddContent(5, 
#nullable restore
#line 8 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\Pages\Autentica\Logout.razor"
         Mensagem

#line default
#line hidden
#nullable disable
                );
                __builder2.CloseElement();
            }
            ));
            __builder.CloseComponent();
        }
        #pragma warning restore 1998
#nullable restore
#line 12 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\Pages\Autentica\Logout.razor"
       
    bool Exibir = false;

    [Parameter]
    public string Mensagem { get; set; }

    protected async override Task OnInitializedAsync()
    {
        try
        {
            await authStateProvider.Logout();
            navigation.NavigateTo("/");
        }
        catch (Exception ex)
        {
            Exibir = true;
            Mensagem = $"Não foi possível realizar o logout. Erro {ex.Message}";
        }
    }

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private TokenAuthenticationProvider authStateProvider { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private NavigationManager navigation { get; set; }
    }
}
#pragma warning restore 1591
