#pragma checksum "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\Pages\Produto\ProdutoForm.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "18783ba055d535c7484de9836602b6296f362dd5"
// <auto-generated/>
#pragma warning disable 1591
namespace Catalogo_Blazor.Client.Pages.Produto
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.WebAssembly.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using Catalogo_Blazor.Client;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using Catalogo_Blazor.Client.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using Catalogo_Blazor.Shared.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 14 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using Catalogo_Blazor.Client.Auth;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using System.Net.Http.Headers;

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using System.Text;

#line default
#line hidden
#nullable disable
#nullable restore
#line 17 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using System.Text.Json;

#line default
#line hidden
#nullable disable
    public partial class ProdutoForm : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenComponent<Microsoft.AspNetCore.Components.Forms.EditForm>(0);
            __builder.AddAttribute(1, "Model", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Object>(
#nullable restore
#line 2 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\Pages\Produto\ProdutoForm.razor"
                  produto

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(2, "OnValidSubmit", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<Microsoft.AspNetCore.Components.Forms.EditContext>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Forms.EditContext>(this, 
#nullable restore
#line 2 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\Pages\Produto\ProdutoForm.razor"
                                           OnValidSubmit

#line default
#line hidden
#nullable disable
            )));
            __builder.AddAttribute(3, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment<Microsoft.AspNetCore.Components.Forms.EditContext>)((context) => (__builder2) => {
                __builder2.OpenComponent<Microsoft.AspNetCore.Components.Forms.DataAnnotationsValidator>(4);
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(5, "\r\n    ");
                __builder2.OpenComponent<Microsoft.AspNetCore.Components.Forms.ValidationSummary>(6);
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(7, "\r\n    ");
                __builder2.OpenElement(8, "div");
                __builder2.AddAttribute(9, "class", "form-group");
                __builder2.AddMarkupContent(10, "<label form=\"nome\">Nome:</label>\r\n        ");
                __builder2.OpenElement(11, "div");
                __builder2.OpenComponent<Microsoft.AspNetCore.Components.Forms.InputText>(12);
                __builder2.AddAttribute(13, "id", "txtProdutoNome");
                __builder2.AddAttribute(14, "class", "form-control");
                __builder2.AddAttribute(15, "Value", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 8 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\Pages\Produto\ProdutoForm.razor"
                                                         produto.Nome

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(16, "ValueChanged", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<System.String>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<System.String>(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => produto.Nome = __value, produto.Nome))));
                __builder2.AddAttribute(17, "ValueExpression", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Linq.Expressions.Expression<System.Func<System.String>>>(() => produto.Nome));
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(18, "\r\n            ");
                __Blazor.Catalogo_Blazor.Client.Pages.Produto.ProdutoForm.TypeInference.CreateValidationMessage_0(__builder2, 19, 20, 
#nullable restore
#line 9 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\Pages\Produto\ProdutoForm.razor"
                                      () => produto.Nome

#line default
#line hidden
#nullable disable
                );
                __builder2.CloseElement();
                __builder2.CloseElement();
                __builder2.AddMarkupContent(21, "\r\n    ");
                __builder2.OpenElement(22, "div");
                __builder2.AddAttribute(23, "class", "form-group");
                __builder2.AddMarkupContent(24, "<label form=\"descricao\">Descrição:</label>\r\n        ");
                __builder2.OpenElement(25, "div");
                __builder2.OpenComponent<Microsoft.AspNetCore.Components.Forms.InputTextArea>(26);
                __builder2.AddAttribute(27, "class", "form-control");
                __builder2.AddAttribute(28, "Value", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 15 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\Pages\Produto\ProdutoForm.razor"
                                         produto.Descricao

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(29, "ValueChanged", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<System.String>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<System.String>(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => produto.Descricao = __value, produto.Descricao))));
                __builder2.AddAttribute(30, "ValueExpression", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Linq.Expressions.Expression<System.Func<System.String>>>(() => produto.Descricao));
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(31, "\r\n            ");
                __Blazor.Catalogo_Blazor.Client.Pages.Produto.ProdutoForm.TypeInference.CreateValidationMessage_1(__builder2, 32, 33, 
#nullable restore
#line 16 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\Pages\Produto\ProdutoForm.razor"
                                      () => produto.Descricao

#line default
#line hidden
#nullable disable
                );
                __builder2.CloseElement();
                __builder2.CloseElement();
                __builder2.AddMarkupContent(34, "\r\n    ");
                __builder2.OpenElement(35, "div");
                __builder2.AddAttribute(36, "class", "form-group");
                __builder2.AddMarkupContent(37, "<label form=\"descricao\">Preço:</label>\r\n        ");
                __builder2.OpenElement(38, "div");
                __Blazor.Catalogo_Blazor.Client.Pages.Produto.ProdutoForm.TypeInference.CreateInputNumber_2(__builder2, 39, 40, "form-control", 41, 
#nullable restore
#line 22 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\Pages\Produto\ProdutoForm.razor"
                                       produto.Preco

#line default
#line hidden
#nullable disable
                , 42, Microsoft.AspNetCore.Components.EventCallback.Factory.Create(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => produto.Preco = __value, produto.Preco)), 43, () => produto.Preco);
                __builder2.AddMarkupContent(44, "\r\n            ");
                __Blazor.Catalogo_Blazor.Client.Pages.Produto.ProdutoForm.TypeInference.CreateValidationMessage_3(__builder2, 45, 46, 
#nullable restore
#line 23 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\Pages\Produto\ProdutoForm.razor"
                                      () => produto.Preco

#line default
#line hidden
#nullable disable
                );
                __builder2.CloseElement();
                __builder2.CloseElement();
                __builder2.AddMarkupContent(47, "\r\n    ");
                __builder2.OpenElement(48, "div");
                __builder2.AddAttribute(49, "class", "form-group");
                __builder2.AddMarkupContent(50, "<label form=\"descricao\">Categoria:</label>\r\n        ");
                __builder2.OpenElement(51, "div");
                __builder2.OpenElement(52, "select");
                __builder2.AddAttribute(53, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.ChangeEventArgs>(this, 
#nullable restore
#line 29 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\Pages\Produto\ProdutoForm.razor"
                               CategoriaSelectionChanged

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(54, "class", "form-control");
#nullable restore
#line 30 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\Pages\Produto\ProdutoForm.razor"
                 if(categorias != null)
                {
                    

#line default
#line hidden
#nullable disable
#nullable restore
#line 32 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\Pages\Produto\ProdutoForm.razor"
                     foreach(var categoria in categorias)
                    {

#line default
#line hidden
#nullable disable
                __builder2.OpenElement(55, "option");
                __builder2.AddAttribute(56, "value", 
#nullable restore
#line 34 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\Pages\Produto\ProdutoForm.razor"
                                        categoria.CategoriaId

#line default
#line hidden
#nullable disable
                );
                __builder2.AddAttribute(57, "selected", 
#nullable restore
#line 34 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\Pages\Produto\ProdutoForm.razor"
                                                                           categoria.CategoriaId == produto.CategoriaId ? true : false

#line default
#line hidden
#nullable disable
                );
                __builder2.AddContent(58, 
#nullable restore
#line 35 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\Pages\Produto\ProdutoForm.razor"
                             categoria.Nome

#line default
#line hidden
#nullable disable
                );
                __builder2.CloseElement();
#nullable restore
#line 37 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\Pages\Produto\ProdutoForm.razor"
                    }

#line default
#line hidden
#nullable disable
#nullable restore
#line 37 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\Pages\Produto\ProdutoForm.razor"
                     
                }

#line default
#line hidden
#nullable disable
                __builder2.CloseElement();
                __builder2.CloseElement();
                __builder2.CloseElement();
                __builder2.AddMarkupContent(59, "\r\n    ");
                __builder2.OpenElement(60, "div");
                __builder2.AddAttribute(61, "class", "form-group");
                __builder2.OpenComponent<Catalogo_Blazor.Client.Shared.InputImagem>(62);
                __builder2.AddAttribute(63, "Label", "Imagem");
                __builder2.AddAttribute(64, "ImagemSelecionada", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<System.String>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<System.String>(this, 
#nullable restore
#line 43 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\Pages\Produto\ProdutoForm.razor"
                                                        ImagemSelecionada

#line default
#line hidden
#nullable disable
                )));
                __builder2.AddAttribute(65, "ImagemRemota", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 43 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\Pages\Produto\ProdutoForm.razor"
                                                                                          ImagemRemota

#line default
#line hidden
#nullable disable
                ));
                __builder2.CloseComponent();
                __builder2.CloseElement();
                __builder2.AddMarkupContent(66, "\r\n    <br>\r\n    ");
                __builder2.OpenElement(67, "button");
                __builder2.AddAttribute(68, "type", "submit");
                __builder2.AddAttribute(69, "class", "btn btn-success");
                __builder2.AddContent(70, 
#nullable restore
#line 48 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\Pages\Produto\ProdutoForm.razor"
                                                   ButtonTextSubmit

#line default
#line hidden
#nullable disable
                );
                __builder2.CloseElement();
                __builder2.AddMarkupContent(71, "\r\n    ");
                __builder2.OpenElement(72, "button");
                __builder2.AddAttribute(73, "class", "btn btn-danger");
                __builder2.AddAttribute(74, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 49 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\Pages\Produto\ProdutoForm.razor"
                                               ()=>navigation.NavigateTo("categoria")

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddContent(75, 
#nullable restore
#line 49 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\Pages\Produto\ProdutoForm.razor"
                                                                                         ButtonTextCancelar

#line default
#line hidden
#nullable disable
                );
                __builder2.CloseElement();
            }
            ));
            __builder.CloseComponent();
        }
        #pragma warning restore 1998
#nullable restore
#line 52 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\Pages\Produto\ProdutoForm.razor"
       
    List<Categoria> categorias;

    [Inject]
    public NavigationManager navigation { get; set; }

    [Inject]
    public HttpClient http { get; set; }

    [Parameter]
    public Produto produto { get; set; }

    [Parameter]
    public string ButtonTextSubmit { get; set; } = "Salvar";

    [Parameter]
    public string ButtonTextCancelar { get; set; } = "Cancelar";

    [Parameter]
    public EventCallback OnValidSubmit { get; set; }

    [Parameter]
    public string ImagemRemota { get; set; }

    private void ImagemSelecionada(string imagemBase64)
    {
        produto.ImagemUrl = imagemBase64;
        ImagemRemota = null;
        produto.CategoriaId = categorias[0].CategoriaId;
    }

    protected async override Task OnInitializedAsync()
    {
        await CarregaCategorias();
    }

    private async Task CarregaCategorias()
    {
        categorias = await http.GetFromJsonAsync<List<Categoria>>("api/categoria/todas");
    }

    void CategoriaSelectionChanged(ChangeEventArgs e)
    {
        if(int.TryParse(e.Value.ToString(), out int id))
        {
            produto.CategoriaId = id;
        }
    }

#line default
#line hidden
#nullable disable
    }
}
namespace __Blazor.Catalogo_Blazor.Client.Pages.Produto.ProdutoForm
{
    #line hidden
    internal static class TypeInference
    {
        public static void CreateValidationMessage_0<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg0)
        {
        __builder.OpenComponent<global::Microsoft.AspNetCore.Components.Forms.ValidationMessage<TValue>>(seq);
        __builder.AddAttribute(__seq0, "For", __arg0);
        __builder.CloseComponent();
        }
        public static void CreateValidationMessage_1<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg0)
        {
        __builder.OpenComponent<global::Microsoft.AspNetCore.Components.Forms.ValidationMessage<TValue>>(seq);
        __builder.AddAttribute(__seq0, "For", __arg0);
        __builder.CloseComponent();
        }
        public static void CreateInputNumber_2<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, System.Object __arg0, int __seq1, TValue __arg1, int __seq2, global::Microsoft.AspNetCore.Components.EventCallback<TValue> __arg2, int __seq3, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg3)
        {
        __builder.OpenComponent<global::Microsoft.AspNetCore.Components.Forms.InputNumber<TValue>>(seq);
        __builder.AddAttribute(__seq0, "class", __arg0);
        __builder.AddAttribute(__seq1, "Value", __arg1);
        __builder.AddAttribute(__seq2, "ValueChanged", __arg2);
        __builder.AddAttribute(__seq3, "ValueExpression", __arg3);
        __builder.CloseComponent();
        }
        public static void CreateValidationMessage_3<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg0)
        {
        __builder.OpenComponent<global::Microsoft.AspNetCore.Components.Forms.ValidationMessage<TValue>>(seq);
        __builder.AddAttribute(__seq0, "For", __arg0);
        __builder.CloseComponent();
        }
    }
}
#pragma warning restore 1591
