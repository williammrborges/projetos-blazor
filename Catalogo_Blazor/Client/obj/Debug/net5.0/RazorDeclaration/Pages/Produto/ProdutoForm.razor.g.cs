// <auto-generated/>
#pragma warning disable 1591
#pragma warning disable 0414
#pragma warning disable 0649
#pragma warning disable 0169

namespace Catalogo_Blazor.Client.Pages.Produto
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.WebAssembly.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using Catalogo_Blazor.Client;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using Catalogo_Blazor.Client.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using Catalogo_Blazor.Shared.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 14 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using Catalogo_Blazor.Client.Auth;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using System.Net.Http.Headers;

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using System.Text;

#line default
#line hidden
#nullable disable
#nullable restore
#line 17 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\_Imports.razor"
using System.Text.Json;

#line default
#line hidden
#nullable disable
    public partial class ProdutoForm : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
        }
        #pragma warning restore 1998
#nullable restore
#line 52 "C:\Users\william\Desktop\Projetos\Catalogo_Blazor\Client\Pages\Produto\ProdutoForm.razor"
       
    List<Categoria> categorias;

    [Inject]
    public NavigationManager navigation { get; set; }

    [Inject]
    public HttpClient http { get; set; }

    [Parameter]
    public Produto produto { get; set; }

    [Parameter]
    public string ButtonTextSubmit { get; set; } = "Salvar";

    [Parameter]
    public string ButtonTextCancelar { get; set; } = "Cancelar";

    [Parameter]
    public EventCallback OnValidSubmit { get; set; }

    [Parameter]
    public string ImagemRemota { get; set; }

    private void ImagemSelecionada(string imagemBase64)
    {
        produto.ImagemUrl = imagemBase64;
        ImagemRemota = null;
        produto.CategoriaId = categorias[0].CategoriaId;
    }

    protected async override Task OnInitializedAsync()
    {
        await CarregaCategorias();
    }

    private async Task CarregaCategorias()
    {
        categorias = await http.GetFromJsonAsync<List<Categoria>>("api/categoria/todas");
    }

    void CategoriaSelectionChanged(ChangeEventArgs e)
    {
        if(int.TryParse(e.Value.ToString(), out int id))
        {
            produto.CategoriaId = id;
        }
    }

#line default
#line hidden
#nullable disable
    }
}
#pragma warning restore 1591
