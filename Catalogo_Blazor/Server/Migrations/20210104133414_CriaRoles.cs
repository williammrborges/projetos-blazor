﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Catalogo_Blazor.Server.Migrations
{
    public partial class CriaRoles : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "34b8f715-02df-4a28-93bd-14ea3fb21057", "4318b040-2caf-4f17-bf6b-da155bbaf667", "User", "USER" },
                    { "20e82dd1-530d-4681-9feb-16981a3f9daf", "b8aff845-5c19-4b07-b15f-100edd7e051b", "Admin", "ADMIN" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "20e82dd1-530d-4681-9feb-16981a3f9daf");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "34b8f715-02df-4a28-93bd-14ea3fb21057");
        }
    }
}
