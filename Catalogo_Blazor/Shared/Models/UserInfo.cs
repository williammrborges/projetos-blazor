﻿using System.ComponentModel.DataAnnotations;

namespace Catalogo_Blazor.Shared.Models
{
    public class UserInfo
    {
        [Required(ErrorMessage ="Informe o e-mail")]
        [EmailAddress(ErrorMessage ="Formato do e-mail inválido")]
        public string Email { get; set; }

        [Required(ErrorMessage ="Informe a senha")]
        public string Password { get; set; }
    }
}
